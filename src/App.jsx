import React from "react";

import { Switch, Route, Redirect } from "react-router-dom";

import NavBar from "./components/NavBar";
import Dashboard from "./pages/Dashboard";
import ReadMe from "./pages/ReadMe";

function App() {
  return (
    <>
      <NavBar />
      <Switch>
        {/* Main app route */}
        <Route path="/" exact>
          <Dashboard />
        </Route>
        {/* Readme route */}
        <Route path="/readme">
          <ReadMe />
        </Route>
        <Redirect to="/" />
      </Switch>
    </>
  );
}

export default App;
