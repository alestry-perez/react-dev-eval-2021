import React from "react";

import { Link } from "react-router-dom";

export default function NavBar() {
  return (
    <>
      <div className="navbar bg-primary shadow-md ">
        <div className="flex-1">
          <h1 className="Site-Title">SlapSticker</h1>
        </div>
        <div className="flex-none">
          <ul className="menu menu-horizontal p-0">
            <li className="btn-outline">
              <Link to="/">home</Link>
            </li>
            <li className="btn-outline">
              <Link to="/readme">readme</Link>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}
