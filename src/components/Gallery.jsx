import React from "react";

export default function Gallery(props) {
  return (
    <section className="Gallery">
      {props.picture && (
        <div className="Picture">
          <img src={props.picture.dataUri} alt="" />
          <h3>{props.picture.title}</h3>
        </div>
      )}
    </section>
  );
}
