import React from "react";

export default function WebCam(props) {
  return (
    <section className="Main">
      <video ref={props.handleVideoRef} />
      <canvas
        ref={props.handleCanvasRef}
        width={2}
        height={2}
        onClick={props.handleCapture}
      />
    </section>
  );
}
