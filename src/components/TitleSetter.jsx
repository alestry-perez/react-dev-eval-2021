import React from "react";

export default function TitleSetter(props) {
  return (
    <section>
      <div className="form-control">
        <label className="input-group input-group-md">
          <span>Give it a name</span>
          <input
            type="text"
            value={props.title}
            onChange={(ev) => props.setTitle(ev.target.value)}
            className="Title-Input"
          />
        </label>
      </div>
    </section>
  );
}
