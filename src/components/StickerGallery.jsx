import React from "react";

import IMAGES from "../images";

const stickers = (url) => {
  const img = document.createElement("img");
  img.src = url;
  return { img, url };
};

// console.log(stickers);

export default function StickerGallery(props) {
  return (
    <>
      <section className="Stickers">
        {IMAGES &&
          IMAGES.map((item) => (
            <div
              key={item.id}
              className="Sticker-Tape"
              onClick={() => props.setSticker(stickers(item.image))}
            >
              <img src={item.image} alt="" />
            </div>
          ))}
      </section>
    </>
  );
}
