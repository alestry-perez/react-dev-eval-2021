const IMAGES = [
  {
    id: 1,
    image: "/src/images/drake.png",
  },
  {
    id: 2,
    image: "/src/images/is_this_meme.png",
  },
  {
    id: 3,
    image: "/src/images/jake_the_dog.png",
  },
  {
    id: 4,
    image: "/src/images/pikachu.png",
  },
  {
    id: 5,
    image: "/src/images/pizza.png",
  },
  {
    id: 6,
    image: "/src/images/pow.png",
  },
  {
    id: 7,
    image: "/src/images/rainbow.png",
  },
  {
    id: 8,
    image: "/src/images/slap.png",
  },
  {
    id: 9,
    image: "/src/images/sparkles.png",
  },
  {
    id: 10,
    image: "/src/images/suprised_meme.png",
  },
  {
    id: 11,
    image: "/src/images/think_meme.png",
  },
  {
    id: 12,
    image: "/src/images/what_meme.png",
  },
];

export default IMAGES;
