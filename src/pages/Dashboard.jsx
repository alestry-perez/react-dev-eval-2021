import React, { useState } from "react";

import { useWebcamCapture } from "../hooks/useWebcamCapture";

import Gallery from "../components/Gallery";
import WebCam from "../components/WebCam";
import StickerGallery from "../components/StickerGallery";
import TitleSetter from "../components/TitleSetter";

export default function Dashboard() {
  // * currently active sticker
  const [sticker, setSticker] = useState();
  // * title for the picture that will be captured
  const [title, setTitle] = useState("SLAPPE!");

  // * webcam behavior hook
  const [
    handleVideoRef, // callback function to set ref for invisible video element
    handleCanvasRef, // callback function to set ref for main canvas element
    handleCapture, // callback function to trigger taking the picture
    picture, // latest captured picture data object
  ] = useWebcamCapture(sticker?.img, title);

  const props = {
    sticker,
    setSticker,
    title,
    setTitle,
    handleVideoRef,
    handleCanvasRef,
    handleCapture,
    picture,
  };
  return (
    <>
      <section className="Dashboard">
        <div className="card shadow-md bg-secondary mt-5">
          <p className="p-3">
            Have you ever said something so dumb, you just wanted to slap
            yourself? Well now you can!
          </p>
        </div>
        <div className="Gallery-Grid">
          <div className="Card-Box row-start-1 col-span-4">
            <p className="text-lg">Step #1:</p>
            <TitleSetter {...props} />
          </div>

          <div className="Card-Box row-start-2 col-span-4 row-span-3">
            {/* <p>Step three: Slap your self!</p> */}
            <WebCam {...props} />
          </div>

          <div className="Card-Box row-span-6 col-start-5 col-span-2 ">
            <p className="text-lg">Step #2:</p>
            <span> select your sticker...</span>
            <StickerGallery {...props} />
          </div>

          <div className="Card-Box row-start-5 row-span-2 col-span-4 ">
            {/* <p className="text-lg">Step 4: Cherish this moment forever</p> */}
            <Gallery {...props} />
          </div>
        </div>
      </section>
    </>
  );
}
